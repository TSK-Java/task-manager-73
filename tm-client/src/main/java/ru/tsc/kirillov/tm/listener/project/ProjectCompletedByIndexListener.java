package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectCompletedByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-completed-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить проект по его индексу.";
    }

    @Override
    @EventListener(condition = "@projectCompletedByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Завершение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(getToken(), index, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

}
