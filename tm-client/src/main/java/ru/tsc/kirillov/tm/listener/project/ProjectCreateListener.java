package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectCreateRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание нового проекта.";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        @NotNull final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        @NotNull final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final ProjectCreateRequest request =
                new ProjectCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getProjectEndpoint().createProject(request);
    }

}
