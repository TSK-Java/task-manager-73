package ru.tsc.kirillov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
