package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ApplicationVersionRequest;
import ru.tsc.kirillov.tm.dto.response.ApplicationVersionResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение версии программы.";
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #event.name || @applicationVersionListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Версия]");
        System.out.println("Клиент: " + getPropertyService().getApplicationVersion());
        ApplicationVersionResponse response = getSystemEndpoint().getVersion(new ApplicationVersionRequest());
        System.out.println("Сервер: " + response.getVersion());
    }

}
