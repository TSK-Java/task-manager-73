package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    @Transactional
    TaskDto create(@Nullable String userId);

    @NotNull
    @Transactional
    TaskDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    @Transactional
    TaskDto save(@Nullable String userId, @Nullable TaskDto task);

    @Nullable
    Collection<TaskDto> findAll(@Nullable String userId);

    @Nullable
    TaskDto findById(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeById(@Nullable String userId, @Nullable String id);

    @Transactional
    void remove(@Nullable String userId, @Nullable TaskDto task);

    @Transactional
    void remove(@Nullable String userId, @Nullable List<TaskDto> tasks);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Transactional
    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
