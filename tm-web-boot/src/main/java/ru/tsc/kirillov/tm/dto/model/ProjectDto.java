package ru.tsc.kirillov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners(AuditingEntityListener.class)
public class ProjectDto extends AbstractWbsDtoModel {

    private static final long serialVersionUID = 2;

    public ProjectDto(@NotNull final String userId, @NotNull final String name) {
        super(userId, name);
    }

    public ProjectDto(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        super(userId, name, description);
    }

    public ProjectDto(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status,
            @Nullable final Date dateBegin
    ) {
        super(userId, name, status, dateBegin);
    }

    public ProjectDto(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(userId, name, description, dateBegin, dateEnd);
    }

}
