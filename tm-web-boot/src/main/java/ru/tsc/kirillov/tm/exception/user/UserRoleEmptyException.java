package ru.tsc.kirillov.tm.exception.user;

public final class UserRoleEmptyException extends AbstractUserException {

    public UserRoleEmptyException() {
        super("Ошибка! Роль пользователя не задана.");
    }

}
