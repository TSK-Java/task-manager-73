package ru.tsc.kirillov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.config.WebApplicationConfiguration;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;
import ru.tsc.kirillov.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class TaskControllerTest {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private final TaskDto taskDelete = new TaskDto("id", "Name");

    @NotNull
    private final TaskDto taskEdit = new TaskDto("id", "Name");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.save(UserUtil.getUserId(), taskDelete);
    }

    @After
    public void clean() {
        taskService.clear(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(2, taskService.count(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String url = String.format("/task/delete/%s", taskDelete.getId());
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, taskService.count(userId));
        Assert.assertTrue(taskService.existsById(userId, taskDelete.getId()));
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(0, taskService.count(userId));
        Assert.assertFalse(taskService.existsById(userId, taskDelete.getId()));
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String url = String.format("/task/edit/%s", taskEdit.getId());
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
