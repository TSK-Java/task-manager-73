package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.kirillov.tm.dto.model.Result;
import ru.tsc.kirillov.tm.dto.model.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    Result login(
            @NotNull
            @WebParam(name = "login") final String login,
            @NotNull
            @WebParam(name = "password") final String password
    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    UserDto profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
