package ru.tsc.kirillov.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.kirillov.tm.listener.LoggerListener;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.tsc.kirillov.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory =
                new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    @NotNull
    public LoggerListener loggerListener() {
        return new LoggerListener();
    }

}
