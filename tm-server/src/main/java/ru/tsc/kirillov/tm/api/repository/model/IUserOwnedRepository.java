package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Transactional
    void deleteByUserId(@Nullable String userId);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    @NotNull
    Page<M> findAllByUserId(@Nullable String userId, @NotNull Pageable pageable);

    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}
