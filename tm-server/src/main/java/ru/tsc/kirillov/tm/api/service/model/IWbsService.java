package ru.tsc.kirillov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.User;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IWbsService<M extends AbstractWbsModel> extends IUserOwnedService<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<M> findAll(final Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable Sort sort);

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M updateByIndex(
            @Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    M create(@Nullable User user, @NotNull String name);

    @Nullable
    M create(@Nullable User user, @NotNull String name, @NotNull String description);

    @Nullable
    M create(
            @Nullable User user,
            @NotNull String name,
            @NotNull String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    );

    @NotNull
    List<M> findAll(@Nullable String userId, @NotNull Comparator<M> comparator);

}
